@extends('layouts.app')

@section('content')


<h1><center><b>Agregar Pelicula</b></center></h1>

<form action="../public/movies" method="post">
	{{ csrf_field() }}
	<center><table border="1" width="50%">
		<tr><td><b><center>Titulo</center></b></td><td><b><center>Autor</center></b></td><td><b><center>Genero</center></b></td><td><b><center>Año</center></b></td><td><b><center>Acción</center></b></td></tr>
		<tr><td><b><center><input type="text" name="titulo" id="titulo" required></center></b></td><td><b><center><input type="text" name="autor" id="autor" required></center></b></td><td><b><center><input type="text" name="genero" id="genero" required></center></b></td><td><b><center><input type="text" pattern="[0-9]{4}" name="ano" id="ano" required></center></b></td><td><b><center><select name="action" id="action">
			<option value="">[Seleccione]</option>
			<option value="1">Creada</option>
		</select></center></b></td></tr>
	</table></center><br>
	<input type="hidden" name="usuario" id="usuario" value={{ Auth::user()->id }}>
	<input type="hidden" name="delete" id="delete" value=0>
	<center><button type="submit" class="btn btn-primary">Agregar</button></center>
</form>

@endsection