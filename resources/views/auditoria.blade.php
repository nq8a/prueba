@extends('layouts.app')

@section('content')

<center><h1>Auditoria</h1></center>
<center><table border="1" width="50%">
<tr>
<td><b>ID</b></td>
<td><b>ID Usuario</b></td>
<td><b>Usuario</b></td>
<td><b>ID Pelicula</b></td>
<td><b>Pelicula</b></td>
<td><b>Acción</b></td>
</tr>

@foreach($audit as $audits)
  <tr>
  <td>{{ $audits->id_audit }}</td>
  <td>{{ $audits->id_usuario }}</td>
  <td>{{ $audits->name }}</td>
  <td>{{ $audits->id_movie }}</td>
  <td>{{ $audits->titulo }}</td>
  <td>{{ $audits->nombre }}</td>

  </tr>
@endforeach
</table></center>
@endsection