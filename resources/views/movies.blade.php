@extends('layouts.app')

@section('content')

<center><h1>Peliculas</h1></center>
<center><table border="1" width="50%">
<tr>
<td><b>Id</b></td>
<td><b>Titulo</b></td>
<td><b>Autor</b></td>
<td><b>Año</b></td>
<td><b>Genero</b></td>
</tr>

@foreach($movie as $movies)
  <tr>
  <td>{{ $movies->id_movie }}</td>
  <td>{{ $movies->titulo }}</td>
  <td>{{ $movies->autor }}</td>
  <td>{{ $movies->año }}</td>
  <td>{{ $movies->genero }}</td>
  </tr>
@endforeach
</table></center>
@endsection