@extends('layouts.app')

@section('content')


<h1><center><b>Modificar Pelicula</b></center></h1>


	{{ csrf_field() }}
<center><h1>Peliculas</h1></center>
<center><table border="1" width="50%">
<tr>
<td><b>Id</b></td>
<td><b>Titulo</b></td>
<td><b>Autor</b></td>
<td><b>Año</b></td>
<td><b>Genero</b></td>
<td><b>Acción</b></td>
</tr>

@foreach($movie as $movies)
  <tr>
  <td>{{ $movies->id_movie }}</td>
  <td>{{ $movies->titulo }}</td>
  <td>{{ $movies->autor }}</td>
  <td>{{ $movies->año }}</td>
  <td>{{ $movies->genero }}</td>
  <td><a href="modificar/{{ $movies->id_movie }}"><button class="btn btn-primary">Editar</button></a><a href="delete/{{ $movies->id_movie }}"><button class="btn btn-danger">Eliminar</button></a></td>
  </tr>
@endforeach
</table></center><br>
@endsection