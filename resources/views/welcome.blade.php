<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/alertify.css') }}">
        <link rel="stylesheet" href="{{ asset('css/themes/semantic.css') }}">
        <title>Inicio</title>
    </head>
    <body bgcolor="#FFD5B9">
        <center><header><h1>Sistema de Video Store</h1></header></center><br><br>
        <section>
            <center><p><h2>Para que fue diseñado</h2></p></center><br><br>
            <center><p><h3>Inicialmente el sistema fue diseñado para realizar un CRUD de las peliculas de una video tienda donde el usuario registrado en este caso un empleado de la video tienda puede agregar, consultar, modificar, deshabilitar las peliculas en la tienda.</h3></p></center><br><br>
        </section>
      <center>  <div>
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <button><a href="{{ url('/home') }}">Home</a></button>
                    @else
                        <button><a href="{{ route('login') }}">Login</a></button>
                        <button><a href="{{ route('register') }}">Register</a></button>
                    @endauth
                </div>
            @endif</center>


    </body>
    <script src="{{ asset('js/jquery.js') }}"></script>
</html>
