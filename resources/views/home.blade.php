@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><center>Acción</center></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table width="100%">
                        <ul width="100%"> 
                            <tr><td width="25%"><center><a href="create"><button class="btn btn-success">Crear</button></a></center></td><td width="25%"><center><a href="movies"><button class="btn btn-primary">Listar</button></a></center></td>
                           <td width="25%"><center><a href="modificar"><button class="btn btn-primary">Modificar</button></a></center></td>
                            <td  width="25%"><center><a href="auditoria"><button class="btn btn-primary">Auditoria</button></a></td></center></tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
