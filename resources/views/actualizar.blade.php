@extends('layouts.app')

@section('content')


<h1><center><b>Modificar Pelicula</b></center></h1>


	
<center><h1>Peliculas</h1></center>
<form method="POST" action="../movies/update">
	<input name="_method" type="hidden"  value="PUT">

{{ csrf_field() }}
<center><table border="1" width="90%">
<tr>
<td><b>Id</b></td>
<td><b>Titulo</b></td>
<td><b>Autor</b></td>
<td><b>Año</b></td>
<td><b>Genero</b></td>
<td><b>Acción</b></td>
</tr>

@foreach($movie as $movies)
  <tr>
  <td>{{ $movies->id_movie }}</td>
  <td><input type="text" name="titulo" id="titulo" requered value="{{ $movies->titulo }}"></td>
  <td><input type="text" name="autor" id="autor" requered value="{{ $movies->autor }}"></td>
  <td><input type="text" name="ano" id="ano" requered value="{{ $movies->año }}"></td>
  <td><input type="text" name="genero" id="genero" requered value="{{ $movies->genero }}"></td>
  <td><input type="submit" class="btn btn-primary" value="Editar"></td>
  </tr>
  <input type="hidden" name="id" id="id" value="{{ $movies->id_movie }}">
  <input type="hidden" name="usuario" id="usuario" value="{{ Auth::user()->id }}">
@endforeach
</table></center></form><br>
@endsection