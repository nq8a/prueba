<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/movies', 'MoviesController');
Route::get('/create', 'MoviesController@create')->name('create');
Route::get('/modificar', 'MoviesController@editar')->name('editar');
Route::get('/modificar/{id}', 'MoviesController@edit')->name('edit');
Route::get('/delete/{id}', 'MoviesController@borrar')->name('borrar');
Route::get('/auditoria', 'AuditoriaController@listar')->name('listar');
