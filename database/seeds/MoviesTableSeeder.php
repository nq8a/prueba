<?php

use Illuminate\Database\Seeder;
use App\Movie;
class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
               DB::table('movies')->where('id_movie', 1)->delete();

        Movie::insert(array(
			array('id_movie' => 1,
			'titulo' => 'CrossFire',
			'genero' => 'Acción',
			'autor' => 'Guillermo del Toro',
			'año' => 1999,
			'delete' => 0),
			array('id_movie' => 2,
			'titulo' => 'Two Face',
			'genero' => 'Comedia',
			'autor' => 'Robert de Niro',
			'año' => 2005,
			'delete' => 0),
			array('id_movie' => 3,
			'titulo' => 'Muerte Lenta',
			'genero' => 'Terror',
			'autor' => 'Jack Russel',
			'año' => 2018,
			'delete' => 0),
			        ));
    }
}
