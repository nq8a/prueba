<?php

use Illuminate\Database\Seeder;
use App\Action;
class ActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('actions')->where('id_action', 1)->delete();

        Action::insert(array(
			array('id_action' => 1,
			'nombre' => 'Creada'),
            array('id_action' => 2,
			'nombre' => 'Modificada'),
            array('id_action' => 3,
			'nombre' => 'Eliminada')
			        ));
    }
}
