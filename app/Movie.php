<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
            protected $dates = [
		'deleted_at'
	];

	    protected $table = 'movies';

	    	public function delete()
    {
        // delete all related itineraries

        return parent::delete();
    }
}
