<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
            protected $dates = [
		'deleted_at'
	];

	    protected $table = 'audits';

	    	public function delete()
    {
        // delete all related itineraries

        return parent::delete();
    }
}

