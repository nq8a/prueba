<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Audit;
use Illuminate\Support\Facades\DB;
class AuditoriaController extends Controller
{
    public function listar(){
	        $audit = DB::table('audits')
        ->select('*')
        ->join('movies','movies.id_movie','=','audits.id_movie')
        ->join('actions','actions.id_action','=','audits.id_action')
        ->join('users','users.id','=','audits.id_usuario')
        ->get();
       return view("auditoria", compact('audit'));
}

}
