<?php

namespace App\Http\Controllers;
use App\Movie;
use App\Audit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movie = DB::table('movies')
        ->select('*')
        ->where('delete',0)
        ->get();
        return view("movies", compact('movie'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();

        $movie = new Movie;
        $movie->titulo = $input['titulo'];
        $movie->genero = $input['genero'];
        $movie->autor = $input['autor'];
        $movie->año = $input['ano'];
        $movie->delete = $input['delete'];

        $movie1 = Movie::all();

        if($movie1){
            $idmovie=$movie1->last()->id_movie;
        }else{
            $idmovie=1;
        }

        $audit = new Audit;
        $audit->id_usuario = $input['usuario'];
        $audit->id_movie = $idmovie;
        $audit->id_action = $input['action'];
        
        $audit -> save();
        $movie -> save();
        return view('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $movie = DB::table('movies')
        ->where('id_movie',$id)
        ->get();
        return view("actualizar", compact('movie'));
    }

        public function editar()
    {
        $movie = DB::table('movies')
        ->get();
        return view("modificar",compact('movie'));
    }


        public function borrar($id)
    {
        $movie = DB::table('movies')
        ->where('id_movie',$id)
        ->get();
        return view("delete",compact('movie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
                $input = $request->all();


           $movie= DB::table('movies')
            ->where('id_movie', $input['id'])
            ->update(['titulo' => $input['titulo'],
                        'genero' => $input['genero'],
                        'autor' => $input['autor'],
                        'año' => $input['ano']
                         ]);


        $audit = new Audit;
        $audit->id_usuario = $input['usuario'];
        $audit->id_movie = $input['id'];
        $audit->id_action = 2;


        $audit -> save();

       return view('welcome');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $input = $request->all();
       
           $movie= DB::table('movies')
            ->where('id_movie', $input['id'])
            ->update([ 'delete' => 1
                         ]);
        $audit = new Audit;
        $audit->id_usuario = $input['usuario'];
        $audit->id_movie = $id;
        $audit->id_action = 3;
        $audit -> save();
 
        return view('welcome');
    }
}
