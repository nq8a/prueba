<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
        protected $dates = [
		'deleted_at'
	];

	    protected $table = 'actions';

	    	public function delete()
    {
        // delete all related itineraries

        return parent::delete();
    }
}
